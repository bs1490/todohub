﻿using MongoDB.Bson.Serialization.Attributes;
using TodoHub.Enums;

namespace TodoHub.Entities
{
    public class User : BaseEntity
    {
        [BsonElement("username")]
        public string? UserName { get; set; }
        [BsonElement("email")]
        public string? UserEmail { get; set; }
        [BsonElement("password")]
        public string? Password { get; set; }
        [BsonElement("role")]
        public UserRole Role { get; set; }
    }
}
