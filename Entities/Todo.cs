﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TodoHub.Entities
{
    public class Todo : BaseEntity
    {
        [BsonElement("title")]
        public string? Title { get; set; }
        [BsonElement("description")]
        public string? Description { get; set; }
        [BsonElement("is_copmplete")]
        public bool IsComplete { get; set; }
        [BsonElement("user_id")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? UserId { get; set; }
        [BsonElement("user")]
        public User? User { get; set; }
    }
}
