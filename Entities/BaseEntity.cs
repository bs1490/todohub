﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TodoHub.Entities
{
    [BsonIgnoreExtraElements]
    public class BaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;
    }
}
