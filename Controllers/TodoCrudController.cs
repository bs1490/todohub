﻿using Microsoft.AspNetCore.Mvc;
using TodoHub.Entities;
using TodoHub.Services;

namespace TodoHub.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class TodoCrudController : ControllerBase
    {
        private ITodoCrudService _todoService;
        public TodoCrudController(ITodoCrudService todoService)
        {
            _todoService = todoService;
        }

        [HttpGet("All")]
        public async Task<IActionResult> TodoList()
        {
            var response = await _todoService.TodoListAsync();
            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> TodoById([FromRoute] string id)
        {
            var response = await _todoService.TodoByIdAsync(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> AddTodo(Todo request)
        {
            await _todoService.AddTodoAsync(request);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateTodo([FromBody] Todo request)
        {
            await _todoService.UpdateTodoAsync(request);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodo([FromRoute] string id)
        {
            await _todoService.DeleteTodoAsync(id);
            return Ok();
        }
    }
}
