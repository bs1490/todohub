﻿using Microsoft.AspNetCore.Mvc;
using TodoHub.Entities;
using TodoHub.Models;
using TodoHub.Services;

namespace TodoHub.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class TodoAggrController : ControllerBase
    {
        private ITodoAggrService _todoAggrService;

        public TodoAggrController(ITodoAggrService todoAggrService)
        {
            _todoAggrService = todoAggrService;
        }

        [HttpGet("ByUserId/{userId}")]
        public async Task<IList<Todo>> TodoByUserIdList(string userId)
        {
            var response = await _todoAggrService.TodoByUserIdListAsync(userId);
            return response;
        }


        [HttpGet("ByUsername/{username}")]
        public async Task<IList<Todo>> TodoByUsernameList(string username)
        {
            var response = await _todoAggrService.TodoByUsernameListAsync(username);
            return response;
        }

        [HttpGet("PagedFilter")]
        public async Task<IList<Todo>> TodoByFilterList([FromQuery] FilterOptions filterOptions)
        {
            var response = await _todoAggrService.TodoFilteredListAsync(filterOptions);
            return response;
        }
    }
}
