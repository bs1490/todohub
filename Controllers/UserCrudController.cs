﻿using Microsoft.AspNetCore.Mvc;
using TodoHub.Entities;
using TodoHub.Services;

namespace TodoHub.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class UserCrudController : ControllerBase
    {
        private readonly IUserCrudService _userService;
        public UserCrudController(IUserCrudService userService)
        {
            _userService = userService;
        }

        [HttpGet("All")]
        public async Task<IActionResult> UserList()
        {
            var response = await _userService.UserListAsync();
            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> UserById([FromRoute] string id)
        {
            var response = await _userService.UserByIdAsync(id);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(User request)
        {
            await _userService.AddUserAsync(request);
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody] User request)
        {
            await _userService.UpdateUserAsync(request);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] string id)
        {
            await _userService.DeleteUserAsync(id);
            return Ok();
        }
    }
}
