﻿using MongoDB.Driver;

namespace TodoHub.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMongoDB(this IServiceCollection services, IConfigurationSection connectionString)
        {
            var client = new MongoClient(connectionString["ConnectionString"]);
            var databaseName = connectionString["DatabaseName"];

            services.AddSingleton<IMongoClient>(client);
            services.AddScoped(provider => client.GetDatabase(databaseName));
        }
    }
}
