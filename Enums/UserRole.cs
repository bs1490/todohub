﻿namespace TodoHub.Enums
{
    public enum UserRole
    {
        Anonymus = 0,
        Admin = 1,
        Regular = 2
    }
}
