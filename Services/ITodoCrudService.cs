﻿using MongoDB.Bson;
using TodoHub.Entities;

namespace TodoHub.Services
{
    public interface ITodoCrudService
    {
        Task<IList<Todo>> TodoListAsync();
        Task<Todo> TodoByIdAsync(string id);
        Task AddTodoAsync(Todo request);
        Task UpdateTodoAsync(Todo request);
        Task DeleteTodoAsync(string id);
    }
}
