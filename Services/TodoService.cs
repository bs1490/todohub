﻿using MongoDB.Bson;
using MongoDB.Driver;
using TodoHub.Entities;
using TodoHub.Repositories;

namespace TodoHub.Services
{
    public class TodoService : ITodoCrudService
    {
        private readonly IBaseRepository<Todo> _todoRepository;
        public TodoService(IBaseRepository<Todo> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<IList<Todo>> TodoListAsync()
        {
            return await _todoRepository.GetAllAsync();
        }
        public async Task<Todo> TodoByIdAsync(string id)
        {
            var result = await _todoRepository.GetByIdAsync(id);
            return result;
        }

        public async Task AddTodoAsync(Todo request)
        {
            await _todoRepository.AddAsync(request);
        }

        public async Task UpdateTodoAsync(Todo request)
        {
            await _todoRepository.UpdateAsync(request);
        }

        public async Task DeleteTodoAsync(string id)
        {
            await _todoRepository.DeleteAsync(id);
        }
    }
}
