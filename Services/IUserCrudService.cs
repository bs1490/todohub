﻿using TodoHub.Entities;

namespace TodoHub.Services
{
    public interface IUserCrudService
    {
        Task<IList<User>> UserListAsync();
        Task<User> UserByIdAsync(string id);
        Task AddUserAsync(User request);
        Task UpdateUserAsync(User request);
        Task DeleteUserAsync(string id);
    }
}
