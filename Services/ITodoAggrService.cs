﻿using TodoHub.Entities;
using TodoHub.Models;

namespace TodoHub.Services
{
    public interface ITodoAggrService
    {
        Task<IList<Todo>> TodoByUserIdListAsync(string userId);
        Task<IList<Todo>> TodoByUsernameListAsync(string username);
        Task<IList<Todo>> TodoFilteredListAsync(FilterOptions filterOptions);
    }
}
