﻿using MongoDB.Bson;
using System.Text.RegularExpressions;
using TodoHub.Entities;
using TodoHub.Models;
using TodoHub.Repositories;

namespace TodoHub.Services
{
    public class TodoAggrService : ITodoAggrService
    {
        private readonly IBaseRepository<Todo> _todoRepository;

        public TodoAggrService(IBaseRepository<Todo> todoRepository)
        {
            _todoRepository = todoRepository;
        }

        public async Task<IList<Todo>> TodoByUserIdListAsync(string userId)
        {
            var pipelines = new List<BsonDocument>
            {
                new BsonDocument
                (
                    "$match",
                    new BsonDocument
                    (
                        "user_id",
                        userId
                    )
                )
            };
            var result = await _todoRepository.ExecuteAggregationAsync(pipelines);
            return result;
        }

        public async Task<IList<Todo>> TodoByUsernameListAsync(string username)
        {
            var pipelines = new List<BsonDocument>
            {
                new BsonDocument
                (
                    "$lookup",
                    new BsonDocument
                    {
                        { "from", "Users" },
                        { "localField", "user_id" },
                        { "foreignField", "_id" },
                        { "as", "user" }
                    }
                ),
                new BsonDocument
                (
                    "$addFields",
                    new BsonDocument
                    (
                        "user",
                        new BsonDocument
                        (
                            "$arrayElemAt",
                            new BsonArray
                            {
                                "$user",
                                0
                            }
                        )
                    )
                ),
                new BsonDocument
                (
                    "$match",
                    new BsonDocument
                    (
                        "user.username",
                        username
                    )
                )
            };

            var result = await _todoRepository.ExecuteAggregationAsync(pipelines);
            return result;
        }

        public async Task<IList<Todo>> TodoFilteredListAsync(FilterOptions filterOptions)
        {
            var pipelines = new List<BsonDocument>();
            if(filterOptions != null)
            {
                if (!string.IsNullOrEmpty(filterOptions.SearchQuery))
                {
                    var regex = new BsonRegularExpression(new Regex (filterOptions.SearchQuery, RegexOptions.IgnoreCase));

                    pipelines.Add(
                        new BsonDocument
                        (
                            "$match", new BsonDocument(
                                "$or", new BsonArray {
                                    new BsonDocument("title", regex),
                                    new BsonDocument("description", regex)
                                }
                            )
                        )
                    );
                }

                if(Math.Abs(filterOptions.OrderBy) == 1)
                {
                    pipelines.Add(
                        new BsonDocument
                        (
                            "$sort",
                            new BsonDocument
                            (
                                "title",
                                filterOptions.OrderBy
                            )
                        )
                    );
                }

                if(filterOptions.PageSize > 0 && filterOptions.PageNumber > 0)
                {
                    int skip = (filterOptions.PageNumber - 1) * filterOptions.PageSize;
                    pipelines.Add(new BsonDocument("$skip", skip));
                    pipelines.Add(new BsonDocument("$limit", filterOptions.PageSize));
                }
            }
            var result = await _todoRepository.ExecuteAggregationAsync(pipelines);
            return result;
        }
    }
}
