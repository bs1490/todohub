﻿using TodoHub.Entities;
using TodoHub.Repositories;

namespace TodoHub.Services
{
    public class UserService : IUserCrudService
    {
        private readonly IBaseRepository<User> _userRepository;

        public UserService(IBaseRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task AddUserAsync(User request)
        {
            await _userRepository.AddAsync(request);
        }

        public async Task DeleteUserAsync(string id)
        {
            await _userRepository.DeleteAsync(id);
        }

        public async Task UpdateUserAsync(User request)
        {
            await _userRepository.UpdateAsync(request);
        }

        public async Task<User> UserByIdAsync(string id)
        {
            return await _userRepository.GetByIdAsync(id);
        }

        public async Task<IList<User>> UserListAsync()
        {
            return await _userRepository.GetAllAsync();
        }
    }
}
