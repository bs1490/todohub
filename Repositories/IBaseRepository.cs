﻿using MongoDB.Bson;
using MongoDB.Driver;
using TodoHub.Entities;

namespace TodoHub.Repositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task<IList<T>> GetAllAsync();
        Task<T> GetByIdAsync(string id);
        Task AddAsync(T entity);
        Task DeleteAsync(string id);
        Task UpdateAsync(T entity);
        Task<List<T>> ExecuteAggregationAsync(List<BsonDocument> pipeline);
    }
}
