﻿using MongoDB.Bson;
using MongoDB.Driver;
using TodoHub.Entities;
using TodoHub.Models;

namespace TodoHub.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        #region Fields

        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<T> _collection;

        #endregion

        #region Ctor

        public BaseRepository(IMongoDatabase database)
        {
            _database = database;
            _collection = _database.GetCollection<T>(DbCollections.GetCollectionName(typeof(T)));
        }

        #endregion

        #region Methods

        public async Task<IList<T>> GetAllAsync()
        {
            return await _collection.Find(_ => true).ToListAsync();
        }
        public async Task<T> GetByIdAsync(string id)
        {
            var result = await _collection.Find(_ => _.Id == id).FirstOrDefaultAsync();

            return result;
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(_ => _.Id, entity.Id);
            await _collection.ReplaceOneAsync(filter, entity);
        }

        public async Task DeleteAsync(string id)
        {
            var filter = Builders<T>.Filter.Eq(_ => _.Id, id);
            await _collection.DeleteOneAsync(filter);
        }

        public async Task<List<T>> ExecuteAggregationAsync(List<BsonDocument> pipeline)
        {
            var pipelineDefinition = new BsonDocumentStagePipelineDefinition<T, T>(pipeline);
            var result = await _collection.Aggregate(pipelineDefinition).ToListAsync();
            return result;
        }

        #endregion
    }
}
