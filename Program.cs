using TodoHub.Extensions;
using TodoHub.Repositories;
using TodoHub.Services;

namespace TodoHub
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Connecting MongoDB through extention AddMongoDB() BuilderServiceExtension method

            builder.Services.AddMongoDB(builder.Configuration.GetSection("ConnectionStrings"));

            builder.Services.AddControllers();

            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            builder.Services.AddScoped<ITodoCrudService, TodoService>();
            builder.Services.AddScoped<IUserCrudService, UserService>();
            builder.Services.AddScoped<ITodoAggrService, TodoAggrService>();

            var app = builder.Build();

            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            //app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
