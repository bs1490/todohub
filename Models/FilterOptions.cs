﻿namespace TodoHub.Models
{
    public class FilterOptions
    {
        public string SearchQuery { get; set; } = "";
        public int OrderBy { get; set; }
        public int PageSize { get; set; } = 10;
        public int PageNumber { get; set; } = 1;

    }
}
