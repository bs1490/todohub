﻿namespace TodoHub.Models
{
    public static class DbCollections
    {
        public static string GetCollectionName(Type type)
        {
            switch (type.Name)
            {
                case "Todo":
                    return "Todos";
                case "User":
                    return "Users";
                default:
                    return "";
            }
        }
    }
}
